use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    
    if args.len() != 2 {
        eprintln!("Usage: {} <numbers separated by commas>", args[0]);
        return;
    }

    let input = &args[1];
    let average = cal_average(input);
    println!("The average of the input data: {}", average);
}

fn cal_average(input: &str) -> String {
    let numbers: Vec<i32> = input
        .split(',') 
        .map(|s| s.trim().parse().expect("Process data failure.")) 
        .collect(); 

    // calculate the average 
    let average: f64 = numbers.iter().sum::<i32>() as f64 / numbers.len() as f64;
    let result = average.to_string();
    return result;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(cal_average("1,2,3,4,5,6"), "3.5");
        assert_eq!(cal_average("4,6,2,8,15"), "7");
    }
}
