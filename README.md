# miniProject8-sm957



### Requirements

Rust Command-Line Tool with Testing

- Rust command-line tool
- Data ingestion/processing
- Unit tests

### Steps

1. Create a new Rust project and use the --bin flag to produce the binary executable for the command line tool.
```
cargo new projectname --bin
```

2. Add all dependencies to Cargo.toml

3. Write mian.rs. Add functions to accept a string as argument and print the average of the data vector.

4. Test the functionality of the tool.
```
cargo run --"1,2,3"
```
5. Add a test mod to test the function automatically.
```
mod tests {
    #[test]
    fn test() {
        assert_eq!(cal_average("1,2,3,4,5,6"), "3.5");
        assert_eq!(cal_average("4,6,2,8,15"), "7");
    }
}
```
And in the terminal, run the test case using cargo test
```
cargo test
```

### Screenshot
- Test the functionality of the tool

![](pic/pic1.png)

- Add a test mod to test the function automatically

![](pic/pic2.png)